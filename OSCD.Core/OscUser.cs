﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OSCD.Core
{
    // TODO Adicionar sincronização de threads
    public class OscUser
    {
        private Dictionary<string, OscSubject> subjects;
        private Dictionary<string, int> subscriptions;

        #region Properties
        public string Id { get; private set; }

        public string[] Subjects
        {
            get
            {
                return subjects.Keys.ToArray();
            }
        }

        public int SubjectsCount
        {
            get
            {
                return subjects.Count;
            }
        }

        public string[] Subscriptions
        {
            get
            {
                return subscriptions.Keys.ToArray();
            }
        }

        public int SubscriptionsCount
        {
            get
            {
                return subscriptions.Count;
            }
        }
        #endregion

        public OscUser()
        {
            Id = Guid.NewGuid().ToString();
            subjects = new Dictionary<string, OscSubject>();
            subscriptions = new Dictionary<string, int>();
        }

        public override string ToString()
        {
            return $"OscUser (ID: {Id} | Subjects Count: {SubjectsCount} | Subscriptions Count: {SubscriptionsCount})";
        }

        #region Subject Related Methods
        internal bool AddSubject(string subject)
        {
            if (HasSubject(subject))
                return false;

            subjects.Add(subject, new OscSubject(subject));
            return true;
        }

        internal bool RemoveSubject(string subject)
        {
            return subjects.Remove(subject);
        }

        public bool HasSubject(string subject)
        {
            return subjects.ContainsKey(subject);
        }

        internal bool HasSubject(OscSubject subject)
        {
            return HasSubject(subject.Address);
        }

        internal OscSubject GetSubject(string subject)
        {
            return subjects.TryGetValue(subject, out OscSubject resultSubject) ? resultSubject : null;
        }
        #endregion

        #region Subscription Related Methods
        internal bool SubscribeSubject(string subject, int port)
        {
            if (HasSubject(subject) || HasSubscription(subject))
                return false;

            subscriptions.Add(subject, port);
            return true;
        }

        public bool UnsubscribeSubject(string subject)
        {
            return subscriptions.Remove(subject);
        }

        public bool HasSubscription(string subject)
        {
            return subscriptions.ContainsKey(subject);
        }

        public int? GetSubscriptionPort(string subject)
        {
            return subscriptions.TryGetValue(subject, out int port) ? (int?)port : null;
        }
        #endregion
    }
}