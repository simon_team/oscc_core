﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OSCD.Core
{
    // TODO Adicionar sincronização de threads
    public class OscDistributor
    {
        protected Dictionary<string, OscUser> users;
        protected Dictionary<string, HashSet<string>> notificationRequests;

        public int UsersCount
        {
            get
            {
                return users.Count;
            }
        }

        public int SubjectsCount
        {
            get
            {
                return users.Values.Select(user => user.SubjectsCount).Sum();
            }
        }

        public int SubscriptionsCount
        {
            get
            {
                return users.Values.Select(user => user.SubscriptionsCount).Sum();
            }
        }

        public int NotificationRequestsCount
        {
            get
            {
                return notificationRequests.Values.Sum(value => value.Count);
            }
        }

        public OscDistributor()
        {
            users = new Dictionary<string, OscUser>();
            notificationRequests = new Dictionary<string, HashSet<string>>();
        }

        #region User Management
        public bool RegisterUser(OscUser user)
        {
            if (HasUser(user.Id))
                return false;

            if (user.SubjectsCount != 0 || user.SubscriptionsCount != 0)
                throw new ArgumentException("The received user must not have neither subjects or subscriptions");

            users.Add(user.Id, user);
            return true;
        }

        public bool UnregisterUser(OscUser user)
        {
            if (!users.Remove(user.Id))
                return false;

            foreach (string subject in user.Subjects)
                UnregisterSubject(user, subject);

            foreach (string notificationSubject in notificationRequests.Keys.ToArray())
                DisableSubjectNotification(notificationSubject, user.Id);

            return true;
        }

        public bool HasUser(string userId)
        {
            return users.ContainsKey(userId);
        }

        public OscUser GetUser(string userId)
        {
            return users.TryGetValue(userId, out OscUser user) ? user : null;
        }

        public List<OscUser> GetAllSubscribedUsers(string subject)
        {
            return users.Values.Where(user => user.HasSubscription(subject)).ToList();
        }
        #endregion

        #region Subject Management
        public bool RegisterSubject(string userId, string subject)
        {
            if (userId == null || subject == null)
                throw new ArgumentNullException("None of the received parameters can be null");

            OscUser user = GetUser(userId);
            if (user == null)
                return false;

            return RegisterSubject(user, subject);
        }

        public bool RegisterSubject(OscUser user, string subject)
        {
            if (user == null || subject == null)
                throw new ArgumentNullException("None of the received parameters can be null");

            return !HasSubject(subject) && user.AddSubject(subject);
        }

        public bool UnregisterSubject(OscUser user, string subject)
        {
            if (user == null || subject == null)
                throw new ArgumentNullException("None of the received parameters can be null");

            if (!user.HasSubject(subject) || !user.RemoveSubject(subject))
                return false;

            foreach (OscUser currUser in users.Values)
                currUser.UnsubscribeSubject(subject);

            return true;
        }

        public bool HasSubject(string subject)
        {
            foreach (OscUser user in users.Values)
                if (user.HasSubject(subject))
                    return true;

            return false;
        }

        public OscSubject GetSubject(string subjectAddress)
        {
            OscSubject subject;
            foreach (OscUser user in users.Values)
                if ((subject = user.GetSubject(subjectAddress)) != null)
                    return subject;

            return null;
        }

        public List<string> GetAllSubjects()
        {
            List<string> subjects = new List<string>();
            foreach (OscUser user in users.Values)
                subjects.AddRange(user.Subjects);

            return subjects;
        }
        #endregion

        #region Subscription Management
        public bool SubscribeSubject(string subject, OscUser user, int port)
        {
            if (user.HasSubject(subject) || user.HasSubscription(subject) || !HasSubject(subject))
                return false;

            return user.SubscribeSubject(subject, port);
        }

        public bool UnsubscribeSubject(string subject, OscUser user)
        {
            if (!user.HasSubscription(subject))
                return false;

            return user.UnsubscribeSubject(subject);
        }
        #endregion

        #region Notification Management
        public bool EnableSubjectNotification(string subject, string userId)
        {
            if (notificationRequests.TryGetValue(subject, out HashSet<string> users))
            {
                if (users.Contains(userId))
                    return false;
                else
                {
                    users.Add(userId);
                    return true;
                }
            }
            else
            {
                notificationRequests.Add(subject, new HashSet<string>(new string[] { userId }));
                return true;
            }
        }

        public bool DisableSubjectNotification(string subject, string userId)
        {
            if (notificationRequests.TryGetValue(subject, out HashSet<string> users) && users.Remove(userId))
            {
                if (users.Count == 0)
                    notificationRequests.Remove(subject);
            }
            return false;
        }

        public bool RemoveSubjectNotification(string subject)
        {
            return notificationRequests.Remove(subject);
        }

        public HashSet<string> GetSubjectNotificationRequesters(string subject)
        {
            return notificationRequests.TryGetValue(subject, out HashSet<string> requesters) ? requesters : null;
        }
        #endregion
    }
}