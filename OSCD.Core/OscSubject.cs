﻿using System.Text.RegularExpressions;

namespace OSCD.Core
{
    public class OscSubject
    {
        public const string OscAddressRegexPattern = @"^(\/[A-Za-z0-9]+)+$";

        #region Properties
        public string Address { get; private set; }

        public int MessageCount { get; private set; }
        #endregion

        public OscSubject(string address)
        {
            Address = address;
        }

        /// <summary>
        /// Increments the subject's message counter
        /// </summary>
        public void IncrementMessageCount()
        {
            MessageCount++;
        }

        public static bool IsValidOscAddress(string address)
        {
            return address == null ? false : Regex.IsMatch(address, OscAddressRegexPattern);
        }
    }
}