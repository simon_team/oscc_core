﻿using Bespoke.Common.Net;
using Bespoke.Common.Osc;
using OSCD.Core;
using System;
using System.Net;

namespace OSCD.Network
{
    public class OscNetworkUser : OscUser
    {
        #region Attributes
        private OscClient oscClient;

        private ClientDisconnectedHandler onClientDisconnectedHandler;
        #endregion

        #region Properties
        public IPAddress Address
        {
            get
            {
                return ((IPEndPoint)oscClient.Connection.Client.RemoteEndPoint).Address;
            }
        }

        public IPEndPoint EndPoint
        {
            get
            {
                return (IPEndPoint)oscClient.Connection.Client.RemoteEndPoint;
            }
        }
        #endregion

        public OscNetworkUser(OscClient oscClient, ClientDisconnectedHandler onClientDisconnectedHandler) : base()
        {
            if (oscClient == null || onClientDisconnectedHandler == null)
                throw new ArgumentNullException("Neither the client or the handler can be null");
            else if (!oscClient.IsConnected)
                throw new InvalidOperationException("The received connection must be connected");

            this.oscClient = oscClient;
            this.oscClient.Connection.Disconnected += OnClientDisconnected;
            this.onClientDisconnectedHandler = onClientDisconnectedHandler;
        }

        public void OnClientDisconnected(object sender, TcpConnectionEventArgs e)
        {
            onClientDisconnectedHandler(this);
        }

        public bool SendCommand(OscMessage command)
        {
            if (oscClient.IsConnected)
            {
                oscClient.Send(command);
                return true;
            }

            return false;
        }

        public bool HasSameConnection(TcpConnection connection)
        {
            return oscClient.Connection == connection;
        }
    }
}
