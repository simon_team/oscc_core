﻿using OSCD.Network.Configuration.Elements;
using System.Configuration;

namespace OSCD.Network.Configuration
{
    public class OutputAddresses : ConfigurationSection
    {
        #region Variables
        private static ConfigurationProperty responseAddressElement;
        private static ConfigurationProperty subjectNotificationAddressElement;
        private static ConfigurationProperty subjectCancellationAddressElement;
        private static ConfigurationProperty errorAddressElement;

        private static ConfigurationPropertyCollection propertyCollection;
        #endregion

        static OutputAddresses()
        {
            responseAddressElement = new ConfigurationProperty("responseAddress", typeof(OscAddressConfigurationElement), null, ConfigurationPropertyOptions.IsRequired);
            subjectNotificationAddressElement = new ConfigurationProperty("subjectNotificationAddress", typeof(OscAddressConfigurationElement), null, ConfigurationPropertyOptions.IsRequired);
            subjectCancellationAddressElement = new ConfigurationProperty("subjectCancellationAddress", typeof(OscAddressConfigurationElement), null, ConfigurationPropertyOptions.IsRequired);
            errorAddressElement = new ConfigurationProperty("errorAddress", typeof(OscAddressConfigurationElement), null, ConfigurationPropertyOptions.IsRequired);

            propertyCollection = new ConfigurationPropertyCollection();
            propertyCollection.Add(responseAddressElement);
            propertyCollection.Add(subjectNotificationAddressElement);
            propertyCollection.Add(subjectCancellationAddressElement);
            propertyCollection.Add(errorAddressElement);
        }

        #region Properties
        public string ResponseAddress
        {
            get { return ((OscAddressConfigurationElement)base[responseAddressElement]).Value; }
        }

        public string SubjectNotificationAddress
        {
            get { return ((OscAddressConfigurationElement)base[subjectNotificationAddressElement]).Value; }
        }

        public string SubjectCancellationAddress
        {
            get { return ((OscAddressConfigurationElement)base[subjectCancellationAddressElement]).Value; }
        }

        public string ErrorAddress
        {
            get { return ((OscAddressConfigurationElement)base[errorAddressElement]).Value; }
        }

        protected override ConfigurationPropertyCollection Properties
        {
            get
            {
                return propertyCollection;
            }
        }
        #endregion
    }
}