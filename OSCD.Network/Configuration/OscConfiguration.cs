﻿using System.Configuration;

namespace OSCD.Network.Configuration
{
    public sealed class OscConfiguration : ConfigurationSectionGroup
    {
        #region Singleton
        private static OscConfiguration configuration;
        public static OscConfiguration Configuration
        {
            get
            {
                if (configuration == null)
                    configuration =
                        ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).GetSectionGroup("oscd")
                        as OscConfiguration;

                return configuration;
            }
        }
        #endregion

        #region Properties
        [ConfigurationProperty("serverSettings")]
        public ServerSettings ServerSettings
        {
            get { return (ServerSettings)base.Sections["serverSettings"]; }
        }

        [ConfigurationProperty("inputAddresses")]
        public InputAddresses InputAddresses
        {
            get { return (InputAddresses)base.Sections["inputAddresses"]; }
        }

        [ConfigurationProperty("outputAddresses")]
        public OutputAddresses OutputAddresses
        {
            get { return (OutputAddresses)base.Sections["outputAddresses"]; }
        }
        #endregion
    }
}
