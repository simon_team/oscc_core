﻿using OSCD.Core;
using System.Configuration;

namespace OSCD.Network.Configuration.Elements
{
    public class OscAddressConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("value", IsRequired = true, DefaultValue = "/OSCD/InvalidAddress")]
        [RegexStringValidator(OscSubject.OscAddressRegexPattern)]
        public string Value
        {
            get { return (string)base["value"]; }
        }
    }
}
