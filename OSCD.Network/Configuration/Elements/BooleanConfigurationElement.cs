﻿using System.Configuration;

namespace OSCD.Network.Configuration.Elements
{
    public class BooleanConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("value", IsRequired = true)]
        public bool Value
        {
            get { return (bool)base["value"]; }
        }
    }
}
