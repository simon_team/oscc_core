﻿using System.Configuration;

namespace OSCD.Network.Configuration.Elements
{
    public class UshortConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("value", IsRequired = true)]
        public ushort Value
        {
            get { return (ushort)base["value"]; }
        }
    }
}
