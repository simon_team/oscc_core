﻿using System.Configuration;
using OSCD.Network.Configuration.Elements;

namespace OSCD.Network.Configuration
{
    public class ServerSettings : ConfigurationSection
    {
        #region Variables
        private static ConfigurationProperty serverPortElement;
        private static ConfigurationProperty serverConsumeExceptionsElement;
        private static ConfigurationProperty serverFilterRegisteredMethodsElement;

        private static ConfigurationPropertyCollection propertyCollection;
        #endregion

        static ServerSettings()
        {
            serverPortElement = new ConfigurationProperty(
                "serverPort", typeof(UshortConfigurationElement), null,
                ConfigurationPropertyOptions.IsRequired);
            serverConsumeExceptionsElement = new ConfigurationProperty(
                "serverConsumeExceptions", typeof(BooleanConfigurationElement), null,
                ConfigurationPropertyOptions.IsRequired);
            serverFilterRegisteredMethodsElement = new ConfigurationProperty(
                "serverFilterRegisteredMethods", typeof(BooleanConfigurationElement), null,
                ConfigurationPropertyOptions.IsRequired);

            propertyCollection = new ConfigurationPropertyCollection();
            propertyCollection.Add(serverPortElement);
            propertyCollection.Add(serverConsumeExceptionsElement);
            propertyCollection.Add(serverFilterRegisteredMethodsElement);
        }

        #region Properties
        public ushort ServerPort
        {
            get { return ((UshortConfigurationElement)base[serverPortElement]).Value; }
        }

        public bool ServerConsumeExceptions
        {
            get { return ((BooleanConfigurationElement)base[serverConsumeExceptionsElement]).Value; }
        }

        public bool ServerFilterRegisteredMethods
        {
            get { return ((BooleanConfigurationElement)base[serverFilterRegisteredMethodsElement]).Value; }
        }

        protected override ConfigurationPropertyCollection Properties
        {
            get
            {
                return propertyCollection;
            }
        }
        #endregion
    }
}
