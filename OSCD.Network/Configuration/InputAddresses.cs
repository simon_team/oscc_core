﻿using OSCD.Network.Configuration.Elements;
using System.Configuration;

namespace OSCD.Network.Configuration
{
    public class InputAddresses : ConfigurationSection
    {
        #region Variables
        private static ConfigurationProperty registerUserAddressElement;
        private static ConfigurationProperty unregisterUserAddressElement;
        private static ConfigurationProperty registerSubjectAddressElement;
        private static ConfigurationProperty unregisterSubjectAddressElement;
        private static ConfigurationProperty getAvailableSubjectsAddressElement;
        private static ConfigurationProperty enableSubjectNotificationAddressElement;
        private static ConfigurationProperty disableSubjectNotificationAddressElement;
        private static ConfigurationProperty subscribeSubjectAddressElement;
        private static ConfigurationProperty unsubscribeSubjectAddressElement;

        private static ConfigurationPropertyCollection propertyCollection;
        #endregion

        static InputAddresses()
        {
            registerUserAddressElement = new ConfigurationProperty(
                "registerUserAddress", typeof(OscAddressConfigurationElement), null, ConfigurationPropertyOptions.IsRequired);
            unregisterUserAddressElement = new ConfigurationProperty(
                "unregisterUserAddress", typeof(OscAddressConfigurationElement), null, ConfigurationPropertyOptions.IsRequired);

            registerSubjectAddressElement = new ConfigurationProperty(
                "registerSubjectAddress", typeof(OscAddressConfigurationElement), null, ConfigurationPropertyOptions.IsRequired);
            unregisterSubjectAddressElement = new ConfigurationProperty(
                "unregisterSubjectAddress", typeof(OscAddressConfigurationElement), null, ConfigurationPropertyOptions.IsRequired);

            getAvailableSubjectsAddressElement = new ConfigurationProperty(
                "getAvailableSubjectsAddress", typeof(OscAddressConfigurationElement), null, ConfigurationPropertyOptions.IsRequired);

            enableSubjectNotificationAddressElement = new ConfigurationProperty(
                "enableSubjectNotificationAddress", typeof(OscAddressConfigurationElement), null, ConfigurationPropertyOptions.IsRequired);
            disableSubjectNotificationAddressElement = new ConfigurationProperty(
                "disableSubjectNotificationAddress", typeof(OscAddressConfigurationElement), null, ConfigurationPropertyOptions.IsRequired);

            subscribeSubjectAddressElement = new ConfigurationProperty(
                "subscribeSubjectAddress", typeof(OscAddressConfigurationElement), null, ConfigurationPropertyOptions.IsRequired);
            unsubscribeSubjectAddressElement = new ConfigurationProperty(
                "unsubscribeSubjectAddress", typeof(OscAddressConfigurationElement), null, ConfigurationPropertyOptions.IsRequired);

            propertyCollection = new ConfigurationPropertyCollection();
            propertyCollection.Add(registerUserAddressElement);
            propertyCollection.Add(unregisterUserAddressElement);
            propertyCollection.Add(registerSubjectAddressElement);
            propertyCollection.Add(unregisterSubjectAddressElement);
            propertyCollection.Add(getAvailableSubjectsAddressElement);
            propertyCollection.Add(enableSubjectNotificationAddressElement);
            propertyCollection.Add(disableSubjectNotificationAddressElement);
            propertyCollection.Add(subscribeSubjectAddressElement);
            propertyCollection.Add(unsubscribeSubjectAddressElement);
        }

        #region Properties
        public string RegisterUserAddress
        {
            get { return ((OscAddressConfigurationElement)base[registerUserAddressElement]).Value; }
        }

        public string UnregisterUserAddress
        {
            get { return ((OscAddressConfigurationElement)base[unregisterUserAddressElement]).Value; }
        }

        public string RegisterSubjectAddress
        {
            get { return ((OscAddressConfigurationElement)base[registerSubjectAddressElement]).Value; }
        }

        public string UnregisterSubjectAddress
        {
            get { return ((OscAddressConfigurationElement)base[unregisterSubjectAddressElement]).Value; }
        }

        public string GetAvailableSubjectsAddress
        {
            get { return ((OscAddressConfigurationElement)base[getAvailableSubjectsAddressElement]).Value; }
        }

        public string EnableSubjectNotificationAddress
        {
            get { return ((OscAddressConfigurationElement)base[enableSubjectNotificationAddressElement]).Value; }
        }

        public string DisableSubjectNotificationAddress
        {
            get { return ((OscAddressConfigurationElement)base[disableSubjectNotificationAddressElement]).Value; }
        }

        public string SubscribeSubjectAddress
        {
            get { return ((OscAddressConfigurationElement)base[subscribeSubjectAddressElement]).Value; }
        }

        public string UnsubscribeSubjectAddress
        {
            get { return ((OscAddressConfigurationElement)base[unsubscribeSubjectAddressElement]).Value; }
        }

        protected override ConfigurationPropertyCollection Properties
        {
            get
            {
                return propertyCollection;
            }
        }
        #endregion
    }
}