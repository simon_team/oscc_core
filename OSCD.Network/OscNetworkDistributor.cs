﻿using Bespoke.Common;
using Bespoke.Common.Osc;
using OSCD.Core;
using OSCD.Network.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

namespace OSCD.Network
{
    // TODO Adicionar sincronização de threads
    // TODO Em vez de usar Console.WriteLine usar Console.Out.WriteLineAsync ou https://stackoverflow.com/questions/3670057/does-console-writeline-block
    public class OscNetworkDistributor : OscDistributor
    {
        #region Attributes
        private OscServer tcpOscServer;
        private OscServer udpOscServer;

        private OscConfiguration configuration;

        private ReaderWriterLockSlim dataLock;
        #endregion

        #region Properties
        public ushort ServerPort
        {
            get
            {
                if (!IsRunning || configuration == null)
                    throw new InvalidOperationException("The instance must be running to get the serverPort");

                return configuration.ServerSettings.ServerPort;
            }
        }

        public bool IsRunning
        {
            get
            {
                return tcpOscServer != null && tcpOscServer.IsRunning;
            }
        }
        #endregion

        #region Distributor Management
        private void OnUserDisconnected(OscNetworkUser user)
        {
            dataLock.EnterWriteLock();
            try
            {
                // Prepares the cancellation command that must be ended after the removal
                Dictionary<string, IEnumerable<OscNetworkUser>> cancellationReceivers = PrepareCancellationCommand(user.Subjects);

                // Removes the user and all the data associated with it
                UnregisterUser(user);

                // Sends the cancellation alert
                SendCancellationCommand(cancellationReceivers);
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.StackTrace);
            }
            finally
            {
                Console.WriteLine($"UsersCount: {UsersCount} | NotificationsCount: {NotificationRequestsCount} | SubjectsCount: {SubjectsCount} | SubscriptionsCount: {SubscriptionsCount}");
                dataLock.ExitWriteLock();
            }
        }

        private Dictionary<string, IEnumerable<OscNetworkUser>> PrepareCancellationCommand(IEnumerable<string> subjects)
        {
            dataLock.EnterReadLock();
            Dictionary<string, IEnumerable<OscNetworkUser>> cancellationReceivers = new Dictionary<string, IEnumerable<OscNetworkUser>>();
            try
            {
                // Gets all the subscribers of the user
                foreach (string subject in subjects)
                    cancellationReceivers.Add(subject, GetAllSubscribedUsers(subject).OfType<OscNetworkUser>());
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.StackTrace);
            }
            finally
            {
                dataLock.ExitReadLock();
            }
            return cancellationReceivers;
        }

        private void SendCancellationCommand(Dictionary<string, IEnumerable<OscNetworkUser>> cancellationReceivers)
        {
            // Sends a subject cancellation alert to all the subscribers of the user
            OscMessage cancellationCommand;
            foreach (KeyValuePair<string, IEnumerable<OscNetworkUser>> subscription in cancellationReceivers)
            {
                udpOscServer.UnRegisterMethod(subscription.Key);
                cancellationCommand = new OscMessage(null, configuration.OutputAddresses.SubjectCancellationAddress, subscription.Key);

                foreach (OscNetworkUser currUser in subscription.Value)
                    currUser.SendCommand(cancellationCommand);
            }
        }

        private bool HasUserWithOscClient(OscClient client)
        {
            bool hasUser;
            dataLock.EnterReadLock();
            try
            {
                hasUser = GetUserWithOscClient(client) != null;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.StackTrace);
                hasUser = false;
            }
            finally
            {
                dataLock.ExitReadLock();
            }

            return hasUser;
        }

        private OscNetworkUser GetUserWithOscClient(OscClient client)
        {
            OscNetworkUser networkUser;
            dataLock.EnterReadLock();
            try
            {
                networkUser = users.Values.OfType<OscNetworkUser>().FirstOrDefault(user => user.HasSameConnection(client.Connection));
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.StackTrace);
                networkUser = null;
            }
            finally
            {
                dataLock.ExitReadLock();
            }
            return networkUser;
        }
        #endregion

        #region Server Configuration Getters
        private static OscServer GetTCPServerConfiguration(OscConfiguration configuration,
            EventHandler<OscMessageReceivedEventArgs> messageReceived, EventHandler<ExceptionEventArgs> errorReceived)
        {
            if (configuration == null || messageReceived == null || errorReceived == null)
                throw new ArgumentNullException("None of the method's parameters can be null.");

            OscServer server = new OscServer(TransportType.Tcp, IPAddress.Any, configuration.ServerSettings.ServerPort);

            server.FilterRegisteredMethods = configuration.ServerSettings.ServerFilterRegisteredMethods;
            server.RegisterMethod(configuration.InputAddresses.RegisterUserAddress);
            server.RegisterMethod(configuration.InputAddresses.UnregisterUserAddress);
            server.RegisterMethod(configuration.InputAddresses.RegisterSubjectAddress);
            server.RegisterMethod(configuration.InputAddresses.UnregisterSubjectAddress);
            server.RegisterMethod(configuration.InputAddresses.GetAvailableSubjectsAddress);
            server.RegisterMethod(configuration.InputAddresses.EnableSubjectNotificationAddress);
            server.RegisterMethod(configuration.InputAddresses.DisableSubjectNotificationAddress);
            server.RegisterMethod(configuration.InputAddresses.SubscribeSubjectAddress);
            server.RegisterMethod(configuration.InputAddresses.UnsubscribeSubjectAddress);
            server.MessageReceived += messageReceived;

            server.ConsumeParsingExceptions = configuration.ServerSettings.ServerConsumeExceptions;
            server.ReceiveErrored += errorReceived;

            return server;
        }

        private static OscServer GetUDPServerConfiguration(OscConfiguration configuration,
            EventHandler<OscMessageReceivedEventArgs> messageReceived, EventHandler<ExceptionEventArgs> errorReceived)
        {
            if (configuration == null || messageReceived == null || errorReceived == null)
                throw new ArgumentNullException("None of the method's parameters can be null.");

            OscServer server = new OscServer(TransportType.Udp, IPAddress.Any, configuration.ServerSettings.ServerPort);

            server.FilterRegisteredMethods = configuration.ServerSettings.ServerFilterRegisteredMethods;
            server.MessageReceived += messageReceived;

            server.ConsumeParsingExceptions = configuration.ServerSettings.ServerConsumeExceptions;
            server.ReceiveErrored += errorReceived;

            return server;
        }
        #endregion

        public void Start(OscConfiguration configuration)
        {
            this.configuration = configuration;

            dataLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);

            tcpOscServer = GetTCPServerConfiguration(configuration, CommandReceived, CommandErrorReceived);
            udpOscServer = GetUDPServerConfiguration(configuration, PublicationReceived, PublicationErrorReceived);

            tcpOscServer.Start();
            udpOscServer.Start();
        }

        public void Stop()
        {
            udpOscServer.Stop();
            tcpOscServer.Stop();
        }

        #region Command Receivers
        private void CommandReceived(object sender, OscMessageReceivedEventArgs e)
        {
            if (!TryGetCommandId(e.Message, out int commandId))
            {
                SendErrorMessage(e.Message.Client, HttpStatusCode.BadRequest);
                return;
            }

            string address = e.Message.Address;
            if (address == configuration.InputAddresses.RegisterUserAddress)
                RegisterUserCommandReceived(sender, e, commandId);
            else if (address == configuration.InputAddresses.RegisterSubjectAddress)
                RegisterSubjectCommandReceived(sender, e, commandId);
            else if (address == configuration.InputAddresses.UnregisterSubjectAddress)
                UnregisterSubjectCommandReceived(sender, e, commandId);
            else if (address == configuration.InputAddresses.GetAvailableSubjectsAddress)
                GetAvailableSubjectsCommandReceived(sender, e, commandId);
            else if (address == configuration.InputAddresses.EnableSubjectNotificationAddress)
                EnableSubjectNotificationCommandReceived(sender, e, commandId);
            else if (address == configuration.InputAddresses.DisableSubjectNotificationAddress)
                DisableSubjectNotificationCommandReceived(sender, e, commandId);
            else if (address == configuration.InputAddresses.SubscribeSubjectAddress)
                SubscribeSubjectCommandReceived(sender, e, commandId);
            else if (address == configuration.InputAddresses.UnsubscribeSubjectAddress)
                UnsubscribeSubjectCommandReceived(sender, e, commandId);
            else
                SendErrorMessage(e.Message.Client, HttpStatusCode.NotFound);

            Console.WriteLine($"Command received from {e.Message.SourceEndPoint} with the address {address}");

            dataLock.EnterReadLock();
            try
            {
                Console.WriteLine($"UsersCount: {UsersCount} | NotificationsCount: {NotificationRequestsCount} | SubjectsCount: {SubjectsCount} | SubscriptionsCount: {SubscriptionsCount}");
            }
            catch (Exception exception)
            {
                Console.Error.WriteLine(exception.StackTrace);
            }
            finally
            {
                dataLock.ExitReadLock();
            }
        }

        private void RegisterUserCommandReceived(object sender, OscMessageReceivedEventArgs e, int commandId)
        {
            OscNetworkUser user;
            dataLock.EnterWriteLock();
            try
            {
                if (HasUserWithOscClient(e.Message.Client))
                {
                    SendErrorMessage(e.Message.Client, commandId, HttpStatusCode.BadRequest);
                    return;
                }

                user = new OscNetworkUser(e.Message.Client, OnUserDisconnected);
                RegisterUser(user);
            }
            catch (Exception exception)
            {
                Console.Error.WriteLine(exception.StackTrace);
                return;
            }
            finally
            {
                dataLock.ExitWriteLock();
            }

            OscMessage message = new OscMessage(null, configuration.OutputAddresses.ResponseAddress, commandId);
            message.Append(user.Id);
            user.SendCommand(message);
        }

        private void RegisterSubjectCommandReceived(object sender, OscMessageReceivedEventArgs e, int commandId)
        {
            int nNotificationsSent = 0;
            dataLock.EnterWriteLock();
            try
            {
                // Tries to get the OscNetworkUser associated to the OscClient
                if (!TryGetAndCheckUserData(e.Message, commandId, out OscNetworkUser user))
                    return;

                // Tries to get all the subjects from the received message
                if (!TryGetAndCheckSubjects(e.Message, commandId, out List<string> subjects))
                    return;

                // Tries to register the received subjects
                OscMessage commandResponse = new OscMessage(null, configuration.OutputAddresses.ResponseAddress, commandId);
                int nRegistered = 0;
                foreach (string subject in subjects)
                {
                    if (RegisterSubject(user, subject))
                    {
                        udpOscServer.RegisterMethod(subject);
                        commandResponse.Append((int)configuration.ServerSettings.ServerPort);
                        nRegistered++;
                    }
                    else
                        commandResponse.AppendNil();
                }

                Console.WriteLine($"User {user.Id} have registered {nRegistered} subject{(nRegistered == 1 ? "" : "s")}.");

                // Sends the response
                user.SendCommand(commandResponse);

                // Sends notifications to all the users that required it
                HashSet<string> usersIdToNotify;
                OscUser userToNotify;
                OscMessage notification;
                foreach (string subject in subjects)
                {
                    notification = new OscMessage(null, configuration.OutputAddresses.SubjectNotificationAddress, subject);
                    usersIdToNotify = GetSubjectNotificationRequesters(subject);
                    if (usersIdToNotify != null)
                        foreach (string userIdToNotify in usersIdToNotify)
                            if ((userToNotify = GetUser(userIdToNotify)) == null)
                                continue;
                            else if (((OscNetworkUser)userToNotify).SendCommand(notification))
                                nNotificationsSent++;
                            else
                                break;

                    RemoveSubjectNotification(subject);
                }
            }
            catch (Exception exception)
            {
                Console.Error.WriteLine(exception.StackTrace);
            }
            finally
            {
                dataLock.ExitWriteLock();
            }

            if (nNotificationsSent != 0)
                Console.WriteLine($"{nNotificationsSent} notification{(nNotificationsSent == 1 ? "" : "s")} sent.");
        }

        private void UnregisterSubjectCommandReceived(object sender, OscMessageReceivedEventArgs e, int commandId)
        {
            OscNetworkUser user;
            Dictionary<string, IEnumerable<OscNetworkUser>> cancellationReceivers;
            HashSet<string> unregisterSubjects;
            dataLock.EnterWriteLock();
            try
            {
                // Tries to get the OscNetworkUser associated to the OscClient
                if (!TryGetAndCheckUserData(e.Message, commandId, out user))
                    return;

                // Tries to get all the subjects from the received message
                if (!TryGetAndCheckSubjects(e.Message, commandId, out List<string> subjects))
                    return;

                // Prepares the cancellation command that must be ended after the removal
                cancellationReceivers = PrepareCancellationCommand(subjects);

                // Unregisters the received subjects
                unregisterSubjects = new HashSet<string>();
                foreach (string subject in subjects)
                    if (UnregisterSubject(user, subject))
                        unregisterSubjects.Add(subject);
            }
            catch (Exception exception)
            {
                Console.Error.WriteLine(exception.StackTrace);
                return;
            }
            finally
            {
                dataLock.ExitWriteLock();
            }

            foreach (string subject in cancellationReceivers.Keys.Except(unregisterSubjects))
                cancellationReceivers.Remove(subject);

            // Sends the cancellation alert
            SendCancellationCommand(cancellationReceivers);

            Console.WriteLine($"User {user.Id} unregistered {unregisterSubjects.Count} subject{(unregisterSubjects.Count == 1 ? "" : "s")}.");
        }

        private void GetAvailableSubjectsCommandReceived(object sender, OscMessageReceivedEventArgs e, int commandId)
        {
            // Creates and sends the response
            OscMessage message = new OscMessage(null, configuration.OutputAddresses.ResponseAddress, commandId);
            List<string> subjects;
            dataLock.EnterReadLock();
            try
            {
                subjects = GetAllSubjects();
            }
            catch (Exception exception)
            {
                Console.Error.WriteLine(exception.StackTrace);
                return;
            }
            finally
            {
                dataLock.ExitReadLock();
            }

            foreach (string subject in subjects)
                message.Append(subject);

            e.Message.Client.Send(message);
        }

        private void EnableSubjectNotificationCommandReceived(object sender, OscMessageReceivedEventArgs e, int commandId)
        {
            OscNetworkUser user;
            int nRequestNotifications = 0;
            List<string> toNotify;
            dataLock.EnterWriteLock();
            try
            {
                // Tries to get the OscNetworkUser associated to the OscClient
                if (!TryGetAndCheckUserData(e.Message, commandId, out user))
                    return;

                // Tries to get all the subjects from the received message
                if (!TryGetAndCheckSubjects(e.Message, commandId, out List<string> subjects))
                    return;

                // Tests which of the received subjects are already registered and adds them to the immediate notification list.
                // Also adds the non existent ones to the request list
                toNotify = new List<string>(subjects.Count);
                foreach (string subject in subjects)
                {
                    if (!user.HasSubject(subject))
                    {
                        if (HasSubject(subject))
                            toNotify.Add(subject);
                        else if (EnableSubjectNotification(subject, user.Id))
                            nRequestNotifications++;
                    }
                }
            }
            catch (Exception exception)
            {
                Console.Error.WriteLine(exception.StackTrace);
                return;
            }
            finally
            {
                dataLock.ExitWriteLock();
            }

            Console.WriteLine($"User {user.Id} enabled notifications for {nRequestNotifications} subject{(nRequestNotifications == 1 ? "" : "s")}.");

            // Sends notifications to all the users that required it
            if (toNotify.Count == 0)
                return;

            OscMessage notification;
            int nNotificationsSent = 0;
            foreach (string subject in toNotify)
            {
                notification = new OscMessage(null, configuration.OutputAddresses.SubjectNotificationAddress, subject);
                if (user.SendCommand(notification))
                    nNotificationsSent++;
                else
                    break;
            }

            Console.WriteLine($"{nNotificationsSent} notification{(nNotificationsSent == 1 ? "" : "s")} sent.");
        }

        private void DisableSubjectNotificationCommandReceived(object sender, OscMessageReceivedEventArgs e, int commandId)
        {
            OscNetworkUser user;
            int nDisabledNotifications = 0;
            dataLock.EnterWriteLock();
            try
            {
                // Tries to get the OscNetworkUser associated to the OscClient
                if (!TryGetAndCheckUserData(e.Message, commandId, out user))
                    return;

                // Tries to get all the subjects from the received message
                if (!TryGetAndCheckSubjects(e.Message, commandId, out List<string> subjects))
                    return;

                // Removes the received subjects from the notification list
                foreach (string subject in subjects)
                    if (DisableSubjectNotification(subject, user.Id))
                        nDisabledNotifications++;
            }
            catch (Exception exception)
            {
                Console.Error.WriteLine(exception.StackTrace);
                return;
            }
            finally
            {
                dataLock.ExitWriteLock();
            }

            Console.WriteLine($"User {user.Id} disabled notifications for {nDisabledNotifications} subject{(nDisabledNotifications == 1 ? "" : "s")}.");
        }

        private void SubscribeSubjectCommandReceived(object sender, OscMessageReceivedEventArgs e, int commandId)
        {
            OscNetworkUser user;
            int nSubscribed = 0;
            OscMessage message = new OscMessage(null, configuration.OutputAddresses.ResponseAddress, commandId);
            dataLock.EnterWriteLock();
            try
            {
                // Tries to get the OscNetworkUser associated to the OscClient
                if (!TryGetAndCheckUserData(e.Message, commandId, out user))
                    return;

                // Tests if the received message has the minimum parameters
                IList<object> data = e.Message.Data;
                if (data.Count < 3 || (data.Count - 2) % 3 != 0)
                {
                    SendErrorMessage(e.Message.Client, commandId, HttpStatusCode.BadRequest);
                    return;
                }

                for (int idx = 2; idx < data.Count; idx += 3)
                {
                    // Tests if the current data have the expected type
                    if (!(data[idx] is string) || !(data[idx + 1] is int) || !(data[idx + 2] is bool))
                    {
                        SendErrorMessage(e.Message.Client, commandId, HttpStatusCode.BadRequest);
                        return;
                    }

                    // Tests if the current subject is valid
                    if (!OscSubject.IsValidOscAddress((string)data[idx]))
                    {
                        SendErrorMessage(e.Message.Client, commandId, HttpStatusCode.BadRequest);
                        return;
                    }
                }

                string subject;
                bool canSubscribe;
                for (int idx = 2; idx < data.Count; idx += 3)
                {
                    subject = (string)data[idx];

                    // Tests if user is related to the current subject
                    if (user.HasSubject(subject) || user.HasSubscription(subject))
                        canSubscribe = false;
                    else
                    {
                        // Tries to subscribe to the current subject
                        canSubscribe = SubscribeSubject(subject, user, (int)data[idx + 1]);

                        // Tests if the current subject exists
                        if (canSubscribe)
                            nSubscribed++;
                        // Tests if the user wants to be notified when the current subject becomes available
                        else if ((bool)data[idx + 2])
                            EnableSubjectNotification(subject, user.Id);
                    }
                    // Adds the subscription result to the response message
                    message.Append(canSubscribe);
                }
            }
            catch (Exception exception)
            {
                Console.Error.WriteLine(exception.StackTrace);
                return;
            }
            finally
            {
                dataLock.ExitWriteLock();
            }

            Console.WriteLine($"User {user.Id} subscribed to {nSubscribed} subject{(nSubscribed == 1 ? "" : "s")}.");

            // Sends the response
            user.SendCommand(message);
        }

        private void UnsubscribeSubjectCommandReceived(object sender, OscMessageReceivedEventArgs e, int commandId)
        {
            OscNetworkUser user;
            int nUnsubscriptions = 0;
            dataLock.EnterWriteLock();
            try
            {
                // Tries to get the OscNetworkUser associated to the OscClient
                if (!TryGetAndCheckUserData(e.Message, commandId, out user))
                    return;

                // Tries to get all the subjects from the received message
                if (!TryGetAndCheckSubjects(e.Message, commandId, out List<string> subjects))
                    return;

                // Unsubscribes the user from the received subjects
                foreach (string subject in subjects)
                    if (UnsubscribeSubject(subject, user))
                        nUnsubscriptions++;
            }
            catch (Exception exception)
            {
                Console.Error.WriteLine(exception.StackTrace);
                return;
            }
            finally
            {
                dataLock.ExitWriteLock();
            }

            Console.WriteLine($"User {user.Id} unsubscribed {nUnsubscriptions} subject{(nUnsubscriptions == 1 ? "" : "s")}.");
        }
        #endregion

        #region Publication Receivers
        private void PublicationReceived(object sender, OscMessageReceivedEventArgs e)
        {
            string subject = e.Message.Address;
            string userId = e.Message.Data[0] as string;
            OscUser user;

            dataLock.EnterReadLock();
            try
            {
                if (userId == null || (user = GetUser(userId)) == null)
                {
                    Console.Error.WriteLine("The received user ID isn't valid.");
                    return;
                }

                if (!user.HasSubject(subject))
                {
                    Console.Error.WriteLine("The received user doesn't have the received subject.");
                    return;
                }

                OscMessage message = new OscMessage(null, subject);
                for (int idx = 1; idx < e.Message.Data.Count; idx++)
                    message.Append(e.Message.Data[idx]);

                foreach (OscNetworkUser subscriber in GetAllSubscribedUsers(subject))
                    try
                    {
                        message.Send(new IPEndPoint(subscriber.Address, (int)subscriber.GetSubscriptionPort(subject)));
                    }
                    catch (ObjectDisposedException)
                    {
                    }
            }
            catch (Exception exception)
            {
                Console.Error.WriteLine(exception.StackTrace);
                return;
            }
            finally
            {
                dataLock.ExitReadLock();
            }
        }
        #endregion

        #region Error Receivers
        private void CommandErrorReceived(object sender, ExceptionEventArgs e)
        {
            Console.Error.WriteLine($"Error during reception of command: {e.Exception.GetType()} ({e.Exception.Message})\n{e.Exception.StackTrace}");
        }

        private void PublicationErrorReceived(object sender, ExceptionEventArgs e)
        {
            Console.Error.WriteLine($"Error during reception of publication: {e.Exception.GetType()} ({e.Exception.Message})\n{e.Exception.StackTrace}");
        }
        #endregion

        #region Information Checkers
        private bool IsCommandAddress(string address)
        {
            return (
                address == configuration.InputAddresses.RegisterUserAddress ||
                address == configuration.InputAddresses.UnregisterUserAddress ||
                address == configuration.InputAddresses.RegisterSubjectAddress ||
                address == configuration.InputAddresses.UnregisterSubjectAddress ||
                address == configuration.InputAddresses.GetAvailableSubjectsAddress ||
                address == configuration.InputAddresses.EnableSubjectNotificationAddress ||
                address == configuration.InputAddresses.DisableSubjectNotificationAddress ||
                address == configuration.InputAddresses.SubscribeSubjectAddress ||
                address == configuration.InputAddresses.UnsubscribeSubjectAddress
                );
        }
        #endregion

        #region Information Assertations
        private void AssertMessageDataMinCount(IList<object> data, int minCount)
        {
            if (data.Count < minCount)
                throw new ArgumentException("Invalid format message received.");
        }

        private void AssertMessageDataCount(IList<object> data, int count)
        {
            if (data.Count != count)
                throw new ArgumentException("Invalid format message received.");
        }
        #endregion

        #region Information Getters
        private bool TryGetCommandId(OscMessage message, out int commandId)
        {
            if (message.Data.Count < 1 || !(message.Data[0] is int))
            {
                commandId = 0;
                return false;
            }

            commandId = (int)message.Data[0];
            return true;
        }

        private bool TryGetUserId(OscMessage message, out string userId)
        {
            if (message.Data.Count < 2 || !(message.Data[1] is string))
            {
                userId = string.Empty;
                return false;
            }

            userId = (string)message.Data[1];
            if (!Guid.TryParse(userId, out Guid guid))
            {
                userId = string.Empty;
                return false;
            }

            return true;
        }

        private bool TryGetAndCheckUserData(OscMessage message, int commandId, out OscNetworkUser user)
        {
            // Tries to get the OscNetworkUser associated to the OscClient
            dataLock.EnterReadLock();
            try
            {
                user = GetUserWithOscClient(message.Client);
            }
            catch (Exception exception)
            {
                Console.Error.WriteLine(exception.StackTrace);
                user = null;
            }
            finally
            {
                dataLock.ExitReadLock();
            }

            if (user == null)
            {
                SendErrorMessage(message.Client, commandId, HttpStatusCode.Unauthorized);
                return false;
            }

            // Tries to get the userID from the received message
            if (!TryGetUserId(message, out string userId))
            {
                SendErrorMessage(message.Client, commandId, HttpStatusCode.BadRequest);
                return false;
            }

            // Tests if the received userID is the same that as the one on the associated OscNetworkUser
            if (user.Id != userId)
            {
                SendErrorMessage(message.Client, commandId, HttpStatusCode.Unauthorized);
                return false;
            }

            return true;
        }

        private bool TryGetAndCheckSubjects(OscMessage message, int commandId, out List<string> subjects)
        {
            subjects = null;

            // Tests if the received message has the minimum parameters
            IList<object> data = message.Data;
            if (data.Count < 3)
            {
                SendErrorMessage(message.Client, commandId, HttpStatusCode.BadRequest);
                return false;
            }

            // Tries to get and cast the subjects from the received message
            try
            {
                subjects = data.Skip(2).Cast<string>().ToList();
            }
            catch (InvalidCastException)
            {
                SendErrorMessage(message.Client, commandId, HttpStatusCode.BadRequest);
                return false;
            }

            // Tests if all the received subjects are valid OSC addresses
            if (subjects.Any(subject => !OscSubject.IsValidOscAddress(subject)))
            {
                SendErrorMessage(message.Client, commandId, HttpStatusCode.BadRequest);
                return false;
            }

            return true;
        }

        private int TryGetUserListeningPort(IList<object> data, int idx)
        {
            int? listeningPort = data[idx] as int?;

            if (listeningPort == null)
                throw new FormatException($"The parameter {idx} of the message must be the user's listening port.");
            if (listeningPort < ushort.MinValue || listeningPort > ushort.MaxValue)
                throw new ArgumentOutOfRangeException($"The parameter {idx} of the message must be an integer between {ushort.MinValue} and {ushort.MaxValue}.");

            return (int)listeningPort;
        }
        #endregion

        #region Error Notifiers
        private void SendErrorMessage(OscClient client, HttpStatusCode statusCode)
        {
            OscMessage errorMessage = new OscMessage(null, configuration.OutputAddresses.ErrorAddress, statusCode.GetHashCode());
            client.Send(errorMessage);

            Console.Error.WriteLine($"Error message sent to {client.Connection.Client.LocalEndPoint} with the code {statusCode.GetHashCode()} ({statusCode})");
        }

        private void SendErrorMessage(OscClient client, int commandId, HttpStatusCode statusCode)
        {
            OscMessage errorMessage = new OscMessage(null, configuration.OutputAddresses.ErrorAddress, commandId);
            errorMessage.Append(statusCode.GetHashCode());
            client.Send(errorMessage);

            Console.Error.WriteLine($"Error message sent to {client.Connection.Client.LocalEndPoint} with the command ID {commandId} and the code {statusCode.GetHashCode()} ({statusCode})");
        }
        #endregion

        private static void Main(string[] args)
        {
            Console.SetError(new ErrorWriter());

            OscConfiguration configuration = OscConfiguration.Configuration;
            OscNetworkDistributor networkDistributor = new OscNetworkDistributor();
            networkDistributor.Start(configuration);
            Console.WriteLine("Network Distributor Running...");
            Console.WriteLine("Press ENTER to stop.");

            ConsoleKeyInfo keyInfo;
            do
            {
                keyInfo = Console.ReadKey(true);
            } while (keyInfo.Key != ConsoleKey.Enter);

            networkDistributor.Stop();
            Console.WriteLine("Network Distributor Stoped...");
        }
    }

    #region Error Writer
    class ErrorWriter : TextWriter
    {
        public const ConsoleColor color = ConsoleColor.Red;

        public override Encoding Encoding
        {
            get { return Encoding.Default; }
        }

        public override void Write(string value)
        {
            Console.ForegroundColor = color;
            Console.Out.Write(value);
            Console.ResetColor();
        }

        public override void WriteLine(string value)
        {
            Console.ForegroundColor = color;
            Console.Out.WriteLine(value);
            Console.ResetColor();
        }
    }
    #endregion
}
